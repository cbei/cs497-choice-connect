# cs497-choice-connect

## How to run:
### 1. Set Up (Windows/Mac):
1. Install node.js. You can install node through the following link: https://nodejs.org/en/download/
2. Follow the instructions of the node.js installer.
3. Once the installer has finished, ensure node command works by opening terminal and type the following commands:<br> 
        npm -v<br>
        node -v<br>
If the commands do not work, ensure /usr/bin/local is within your environment path (default installation location).
4. Clone the repository. If git is previously installed and your public ssh key has been uploaded<br>
the repository can be cloned to the current directory using the following command:<br>
        git clone ist-git@git.uwaterloo.ca:cbei/cs497-choice-connect.git<br><br>
If your public ssh key has not been uploaded, either upload an ssh key by following the steps found at<br>
https://jdblischak.github.io/2014-09-18-chicago/novice/git/05-sshkeys.html<br>
or use the following command:<br>
        git clone https://git.uwaterloo.ca/cbei/cs497-choice-connect.git<br><br>
If git has not been installed, either install git by installing git bash through the following website:<br>
        https://git-scm.com/downloads<br>
or the repository can be downloaded (which needs to be uncompressed).
### 2. Running the application:
1. In the terminal, move to the repository folder (using cd commands)
2. Once in the repository folder, run npm install. This will install all the necessary node modules.
3. Once npm install has completed, use the command npm start. This will start the project on localhost:3000.

