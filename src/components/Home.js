import Menu from './Menu.js';
import '../styles/Body.css';
import '../styles/App.css';

function Home() {

  function Start() {
    window.open("https://www.choiceconnect.ca", "_self")
  }
  return (
    <div className="App">
      <Menu current="Homepage"/>
      <div className="Body width">
        <div className="loc-link" id="fr-CA">Utilisez ce site en Français</div>
        <div>
          Choice Connect helps you find your nearest abortion provider based on your needs. The app features detailed referral information for more than 120 abortion providers and clinics across Canada. The web app is anonymous and free to use.
        </div>
        <div>
          <br />
          In order to use the app, some information must be obtained. A pregnancy test and an ultrasound must have been taken prior to using this app. If you have not obtained results from other, please consult with your pharmacy and ultrasound clinic to get the results.
        </div>
        <div>
          <br/>
          Please be aware that due to the recent COVID-19 outbreak, many clinics are adjusting their policies and procedures to ensure the health and safety of both staff and patients. Please reach out to individual abortion providers, but be aware that some clinics are having to change the services they are able to provide.
        </div>
        <div>
          <br/>
          This tool is designed for people who live in Canada. If you live in the United States, please contact the
          <strong> </strong>
          <a href="https://prochoice.org/think-youre-pregnant/find-a-provider/" target="_blank" rel="noreferrer">
            <strong>National Abortion Federation</strong>
          </a>
          <strong>
            .<br/>
          </strong>
          <br/>
          </div>
          <div>
            If you would like your clinic added to Choice Connect or changes made to your listing, please contact choiceconnect@shorecentre.ca.
            <br/>
            <br />
            If you have any questions or concerns, please visit the contacts section to find someone to contact. Visit the locations section if you would like to get in touch with an abortion clinic.
            <br />
            <br/>
            <div>
              &nbsp;If you are concerned about privacy, follow this
              <strong> </strong>
              <a href="https://www.computerhope.com/issues/ch000510.htm" target="_blank" rel="noopener">
                <strong>link</strong>
              </a><strong> </strong>
              to learn how you can clear your browser history.
            </div>
            <br />
            Want to help keep Choice Connect running? Donate today by visiting: <a href="https://www.shorecentre.ca/cc/" target="_blank" rel="noreferrer">www.shorecentre.ca/cc</a>
          </div>
          <div id="nav" className="row-center medium-8-center large-6 columns text-center">
            <input id="next-btn" type="button" className="button" value="Start Now" aria-label="Start Now" onClick={Start}/>
          </div>
      </div>
    </div>
  );
}

export default Home;
