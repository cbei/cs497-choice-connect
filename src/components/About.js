import Menu from "./Menu.js";
import '../styles/Body.css';
import '../styles/Abort.css';

function Abort() {
    return(
        <div className="App">
            <Menu current="About Us" />
            <div className="Body width">
                <div className="et_pb_row et_pb_row_0">
				    <div className="et_pb_column et_pb_column_4_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough et-last-child">
				        <div className="et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_left et_pb_bg_layout_light">
				            <div className="et_pb_text_inner">
                                <h2>
                                    Our&nbsp;Story
                                </h2>
                                <p>
                                    The Sexual Health Options, Resources &amp; Education – SHORE Centre was founded in 1972 and originally named Planned Parenthood Kitchener-Waterloo, followed by Planned Parenthood Waterloo Region. The Centre grew out of the Birth Control Centre at the University of Waterloo in order to provide all residents of our community with sexual health information and support. SHORE Centre’s&nbsp;founders included a librarian, a social worker, a doctor and nurse, Catherine “Kitty” Francis. Watch&nbsp;the video below to hear Kitty Francis tell our founding story.
                                </p>
                                <p>
                                    &nbsp;
                                </p>
                            </div>
			            </div>
                        <div className="et_pb_module et_pb_video et_pb_video_0">
				            <div className="et_pb_video_box">
                                <div className="fluid-width-video-wrapper">
                                    <iframe title="Promoting Choice for 45 Years" src="https://www.youtube.com/embed/4hJyt9hykMY?feature=oembed" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen="" name="fitvid0">
                                    </iframe>
                                </div>
                            </div>
			            </div>
			        </div>	
			    </div>
                <div className="et_pb_row et_pb_row_1">
				    <div className="et_pb_column et_pb_column_2_3 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough">
				        <div className="et_pb_module et_pb_text et_pb_text_1  et_pb_text_align_left et_pb_bg_layout_light">
				            <div className="et_pb_text_inner">
                                <h2>
                                    Our Vision
                                </h2>
                                <p>
                                    SHORE Centre imagines a community that values sexual health and&nbsp;reproductive freedom as essential for people to live their best lives.
                                </p>
                                <h2>
                                    Our Mission
                                </h2>
                                <p>
                                    SHORE Centre offers exceptional and inclusive sexual and reproductive&nbsp;health services in our community that uphold the dignity of everyone.
                                </p>
                                <h2>
                                    Our&nbsp;Values
                                </h2>
                                <p>
                                    <strong>
                                        Accessible:
                                    </strong> We strive to make our programs and services welcoming to all&nbsp;and barrier-free.
                                </p>
                                <p>
                                    <strong>
                                        Inclusive:
                                    </strong> We celebrate diversity and respect the unique lived experiences of&nbsp;every person in our community. We are sex positive and value sexual and&nbsp;gender diversity.
                                </p>
                                <p>
                                    <strong>
                                        Accurate:
                                    </strong> Our programs and services are modeled on scientific evidence&nbsp;and best-practices as well as the lived experience of the people we work&nbsp;with.
                                </p>
                                <p>
                                    <strong>
                                        Courageous:
                                    </strong> We fearlessly advocate for the human right of bodily&nbsp;autonomy and boldly defend choice as we adapt to policy and system&nbsp;changes.
                                    <span className="abort">
                                        &nbsp;
                                    </span>
                                </p>
                                <p>
                                    SHORE Centre is affiliated to the <a href="http://www.ippf.org/">International Planned Parenthood Federation</a> through our membership with <a href="http://www.sexualhealthandrights.ca/">Action Canada for Sexual Health and Rights</a>&nbsp;and are members of the <a href="http://www.nafcanada.org/" target="_blank" rel="noopener noreferrer">National Abortion Federation</a>. For more information about SHORE Centre’s work, please review our <a href="https://www.shorecentre.ca/wp-content/uploads/2020-21-Annual-Report.pdf" target="_blank" rel="noopener noreferrer">Annual Report</a>, <a href="https://www.shorecentre.ca/wp-content/uploads/Strategic-Plan-2020-2023.pdf" rel="attachment wp-att-14801">Strategic Plan, </a>and <a href="https://www.shorecentre.ca/wp-content/uploads/2021-Financial-Statements.pdf">Audited Financial Statements</a>.
                                </p>
                            </div>
			            </div> 
			        </div>
                    <div className="et_pb_column et_pb_column_1_3 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child">
				        <div className="et_pb_module et_pb_image et_pb_image_0 et-animated et_had_animation">
				            <span className="et_pb_image_wrap "><img src="https://www.shorecentre.ca/wp-content/uploads/inclusive.jpg" alt="" title="" height="auto" width="auto" srcSet="https://www.shorecentre.ca/wp-content/uploads/inclusive.jpg 600w, https://www.shorecentre.ca/wp-content/uploads/inclusive-300x227.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" className="wp-image-2050"/></span>
			            </div>
                    <div className="et_pb_module et_pb_text et_pb_text_2  et_pb_text_align_left et_pb_bg_layout_light">
				        <div className="et_pb_text_inner">Our office is barrier-free and interpretation is available upon request.&nbsp;Our programs, services and policies are inclusive of all genders, orientations, abilities, ages and cultures.</div>
			        </div>
                    <div className="et_pb_module et_pb_text et_pb_text_3  et_pb_text_align_left et_pb_bg_layout_light">
				    <div className="et_pb_text_inner"><a href="https://livingwagewr.org/" target="_blank" rel="noopener noreferrer"><img className="size-medium wp-image-986 alignleft" src="https://www.shorecentre.ca/wp-content/uploads/LWWR-Logo-300x229.png" alt="LWWR Logo" width="300" height="229" srcSet="https://www.shorecentre.ca/wp-content/uploads/LWWR-Logo-300x229.png 300w, https://www.shorecentre.ca/wp-content/uploads/LWWR-Logo.png 448w" sizes="(max-width: 300px) 100vw, 300px"/></a><p></p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>We are proud to be a Living Wage Employer.</p></div>
			        </div>
			    </div>	
			</div>
            </div>
        </div>
    )
}

export default Abort;