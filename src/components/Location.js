import Menu from './Menu.js'
import '../styles/Contact.css'
import constants from '../constants/constants'

function newTab(url) {
    window.open(url, "_blank")
}

function Location() {
    var key = 0;
    var contactItems = constants.contactItems.map((tuple) =>
        <div key={key++} className={"small-12 referral-results-clinic " + (tuple.address ? "" : "hidden")}>
            <h3>{tuple.name}</h3>
            {tuple.description === "" ? null : <h5>{tuple.description}</h5>}
            <span>Website: </span>
            <span className="link" onClick={() => newTab(tuple.website)}>
                {tuple.website}
            </span>
            <p className="phone">
                Address: {tuple.address}
            </p>
            <p>
                {tuple.postalCode}
            </p>
            <p>
                {tuple.country}
            </p>
        </div>
    )
    return (
        <div className="App">
            <Menu current="Locations" />
            <div className="Body width">
                <h1>Locations</h1>
                <p>Location of partnered and associated abortion clinics. Visit their websites for more information and ways of getting an appointment.</p>
                <div className="contacts-container">
                    {contactItems}
                </div>
            </div>
        </div>
    )
}

export default Location;