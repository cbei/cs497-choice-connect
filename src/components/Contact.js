import Menu from './Menu.js'
import '../styles/Contact.css'
import constants from '../constants/constants'

function newTab(url) {
    window.open(url, "_blank")
}

function Contact() {
    var key = 0;
    var contactItems = constants.contactItems.map((tuple) =>
        <div key={key++} className="small-12 referral-results-clinic">
            <h3>{tuple.name}</h3>
            {tuple.description === "" ? null : <h5>{tuple.description}</h5>}
            <span>Website: </span>
            <span className="link" onClick={() => newTab(tuple.website)}>
                {tuple.website}
            </span>
            <p className="phone">
                Phone: {tuple.phoneNumber}
            </p>
            <p>
                Email: {tuple.email}
            </p>
        </div>
    )
    return (
        <div className="App">
            <Menu current="Contacts" />
            <div className="Body width">
                <h1>Contacts</h1>
                <p>Contacts for questions or more information. Links on this page will open a new tab. For locations, please visit the locations page.</p>
                <div className="contacts-container">
                    {contactItems}
                </div>
            </div>
        </div>
    )
}

export default Contact;