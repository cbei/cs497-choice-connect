import Menu from './Menu.js';
import '../styles/Abort.css';
import '../styles/App.css';

function Abort() {
    return(
        <div className="App">
            <Menu current="Information" />
            <div className="Body width">
                An abortion is a medical procedure performed to end a pregnancy , which is done in a hospital, clinic or at home with the abortion pill. There are two types of abortion in Canada: abortion pill up to 9 weeks gestation and in-clinic abortion up to 24 weeks gestation.
                <p/>
                <p>
                    Making a decision about what type of abortion is right for you depends on many factors such as how many weeks pregnant you are, where you would like to have your abortion, and who you would like to have with you through the process.
                </p>
                <p>
                    Knowing how many weeks pregnant you are is calculated based on the first day of your last menstrual period or with a dating ultrasound. You will need to know the first day of your last menstrual period or have a dating ultrasound in order to book an abortion.
                </p>
                <p>
                    Abortions are legal, safe, medical procedures covered by OHIP, although some clinics may charge a fee for non-insured services. You do not need a doctor’s referral to have an abortion.
                </p>
                <p>
                    <span className="abort">
                        <a href="https://www.shorecentre.ca/abortion-facts/">
                            Have questions about abortion?&nbsp;Read the&nbsp;facts.
                        </a>
                    </span>
                </p>  
            </div>
        </div>
    )
}

export default Abort