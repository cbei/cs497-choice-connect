import '../styles/Logo.css'
import '../styles/tags.css'
import { Link } from "react-router-dom";

function Logo() {
  return (
    <div id="shore_logo" className="row">
        <div className="small-12 medium-8 large-6 columns">
          <Link to="/">
              <img className="text-center" alt="Choice Connect" src={require('../images/logo_high_res.png')}/>
          </Link>
        </div>
    </div>
  );
}

export default Logo;
