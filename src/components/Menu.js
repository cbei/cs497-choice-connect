import '../styles/Menu.css'
import { Link } from "react-router-dom";
import constants from '../constants/constants';

function Menu(current) {
    var key = 0;
    var listItems = constants.menuItems.map((tuple) =>
        <li key={key++} className={(current.current === tuple.name ? "current" : "false")}>
            <Link to={tuple.path} className={"menuLink "}>
                {tuple.name}
            </Link>
        </li>
    );
    return (
        <div className="menuContainer">
            <ul id="subNavWrapper" className="menu">
                {listItems}
            </ul>
        </div>
    )
}

export default Menu;