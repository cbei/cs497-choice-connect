import '../styles/Panic.css'
import '../styles/tags.css'

function Escape() {
    window.open("https://www.youtube.com", '_self')
}

function Panic() {
    return (
        <div className="panic-button" id="panic-button" onClick={Escape}>
            <div id="panic-button-top">
                <span className="translated-text" data-translate-key="leave">
                Leave
                </span>
            </div>
            <div id="panic-button-description">
                <span className="translated-text full" data-translate-key="leave_details">
                Leave the website by clicking this button.
                </span>
                <span className="translated-text short" data-translate-key="leave_details">
                Leave Website.
                </span>
            </div>
        </div>
    )
}

export default Panic;