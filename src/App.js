import './styles/App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
  
// import Home component
import Home from "./components/Home";
import Logo from './components/Logo';
import Abort from './components/AbortionInfo';
import About from './components/About';
import Contact from './components/Contact';
import Location from './components/Location';

function App() {
  return (
    <>
      <Router>
        <Logo/>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/abort" element={<Abort />} />
          <Route path="/about" element={<About />} />
          <Route path="/contacts" element={<Contact />} />
          <Route path="/locations" element={<Location />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
