const constants = {
    menuItems: [
        {
            name: "Homepage",
            path: "/"
        },
        {
            name: "Information",
            path: "/abort"
        },
        {
            name: "About Us",
            path: "/about"
        },
        {
            name: "Contacts",
            path: "/contacts"
        },
        {
            name: "Locations",
            path: "/locations"
        }
    ],
    contactItems: [
        {
            name: "SHORE Centre",
            website: "https://www.shorecentre.ca/",
            phoneNumber: "519-743-9360",
            email: "info@shorecentre.ca",
            address: "235 King St E #130",
            postalCode: "N2G 4N5",
            country: "Kitchener, Ontario, Canada"
        },
        {
            name: "Sex Sense",
            description: "For Those in BC",
            website: "https://www.optionsforsexualhealth.org/",
            phoneNumber: "1-800-739-7367",
            email: "info@optbc.org",
            address: "3550 EAST HASTINGS ST.",
            postalCode: "V5K 2A7",
            country: "Vancouver, British Columbia, Canada"
        },
        {
            name: "Action Canada for Sexual Health & Rights",
            website: "https://www.actioncanadashr.org/",
            phoneNumber: "1-888-642-2725",
            email: "info@actioncanadaSHR.org"
        },
        {
            name: "National Abortion Federation",
            description: "For those in the US",
            website: "https://prochoice.org/",
            phoneNumber: "202-667-5881",
            email: "naf@prochoice.org",
            address: "1090 Vermont Ave NW #1000",
            postalCode: "DC 20005",
            country: "Washington, United States"
        }
    ]
}

export default constants;